package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.domain.entity.Associate
import com.pucminas.authservice.domain.entity.Person
import com.pucminas.authservice.web.dto.request.AssociateRequest
import com.pucminas.authservice.web.dto.request.AssociateRequestCreate
import com.pucminas.authservice.web.dto.request.AssociateRequestUpdate
import com.pucminas.authservice.web.dto.response.AssociateResponse
import com.pucminas.authservice.web.dto.response.PersonResponse

interface AssociateService : BaseService<AssociateResponse, String> {
    fun save(associateRequest: AssociateRequestCreate): AssociateResponse
    fun edit(id: String, associateRequest: AssociateRequestUpdate): AssociateResponse
    fun findByPersonId(personId: String): PersonResponse
    fun findByPersonUserId(userId: String): PersonResponse
}