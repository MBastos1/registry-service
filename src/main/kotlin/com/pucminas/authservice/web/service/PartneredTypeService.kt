package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.web.dto.response.PartneredTypeResponse

interface PartneredTypeService : BaseService<PartneredTypeResponse, String>