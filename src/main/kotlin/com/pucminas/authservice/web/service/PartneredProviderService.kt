package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredProviderResponse

interface PartneredProviderService : BaseService<PartneredProviderResponse, String> {
    fun save(partneredProviderRequest: PartneredProviderRequestCreate): PartneredProviderResponse
    fun edit(id: String, partneredProviderRequest: PartneredProviderRequestUpdate): PartneredProviderResponse
    fun deleteById(id: String)
}