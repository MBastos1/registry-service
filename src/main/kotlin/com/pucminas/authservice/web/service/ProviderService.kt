package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.domain.entity.Person
import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.web.dto.request.ProviderRequest
import com.pucminas.authservice.web.dto.request.ProviderRequestCreate
import com.pucminas.authservice.web.dto.request.ProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.PersonResponse
import com.pucminas.authservice.web.dto.response.ProviderResponse

interface ProviderService : BaseService<ProviderResponse, String> {
    fun save(providerRequest: ProviderRequestCreate): ProviderResponse
    fun edit(id: String, providerRequest: ProviderRequestUpdate): ProviderResponse
    fun findByPersonId(personId: String): PersonResponse
    fun findByPersonUserId(userId: String): PersonResponse
}