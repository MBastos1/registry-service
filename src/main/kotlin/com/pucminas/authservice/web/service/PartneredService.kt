package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.web.dto.request.PartneredRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredResponse

interface PartneredService : BaseService<PartneredResponse, String> {
    fun save(partneredRequest: PartneredRequestCreate): PartneredResponse
    fun edit(id: String, partneredRequest: PartneredRequestUpdate): PartneredResponse
}