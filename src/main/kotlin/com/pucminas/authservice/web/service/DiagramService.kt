package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.web.dto.request.DiagramRequest
import com.pucminas.authservice.web.dto.response.DiagramResponse

interface DiagramService : BaseService<DiagramResponse, String> {
    fun save(diagramRequest: DiagramRequest): DiagramResponse
    fun edit(id: String, diagramRequest: DiagramRequest): DiagramResponse
    fun deleteById(id: String)
}