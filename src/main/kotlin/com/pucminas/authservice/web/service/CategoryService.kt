package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.domain.entity.Category
import com.pucminas.authservice.web.dto.response.CategoryResponse

interface CategoryService : BaseService<CategoryResponse, String>