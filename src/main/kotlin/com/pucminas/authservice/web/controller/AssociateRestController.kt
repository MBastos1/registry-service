package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.dto.request.AssociateRequest
import com.pucminas.authservice.web.dto.request.AssociateRequestCreate
import com.pucminas.authservice.web.dto.request.AssociateRequestUpdate
import com.pucminas.authservice.web.service.AssociateService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(AssociateRestController.URL)
class AssociateRestController(private val associateService: AssociateService) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(AssociateRestController::class.java)
        const val URL = "/associate"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody associateRequest: AssociateRequestCreate) =
        associateService.save(associateRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody associateRequest: AssociateRequestUpdate) =
        associateService.edit(id, associateRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = associateService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = associateService.findById(id)

    @GetMapping("/person/{personId}")
    @ResponseStatus(HttpStatus.OK)
    fun findByPersonId(@PathVariable personId: String) = associateService.findByPersonId(personId)

    @GetMapping("/person/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun findByUserId(@PathVariable userId: String) = associateService.findByPersonUserId(userId)
}