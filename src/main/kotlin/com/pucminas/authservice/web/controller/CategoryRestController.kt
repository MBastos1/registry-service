package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.service.CategoryService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(CategoryRestController.URL)
class CategoryRestController(private val categoryService: CategoryService) {

    companion object {
        const val URL = "/category"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = categoryService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = categoryService.findById(id)
}