package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.dto.request.DiagramRequest
import com.pucminas.authservice.web.service.DiagramService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(DiagramRestController.URL)
class DiagramRestController(private val diagramService: DiagramService) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(DiagramRestController::class.java)
        const val URL = "/diagram"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody diagramRequest: DiagramRequest) =
        diagramService.save(diagramRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody diagramRequest: DiagramRequest) =
        diagramService.edit(id, diagramRequest)

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findAById(@PathVariable id: String) = diagramService.findById(id)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun findAll() = diagramService.findAll()

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteById(@PathVariable id: String) = diagramService.deleteById(id)

}

