package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.dto.request.PartneredProviderRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestUpdate
import com.pucminas.authservice.web.service.PartneredProviderService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(PartneredProviderRestController.URL)
class PartneredProviderRestController(
    private val partneredProviderService: PartneredProviderService
) {
    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(AssociateRestController::class.java)
        const val URL = "/partnered-provider"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody partneredProviderRequestCreate: PartneredProviderRequestCreate) =
        partneredProviderService.save(partneredProviderRequestCreate)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody partneredProviderRequestUpdate: PartneredProviderRequestUpdate) =
        partneredProviderService.edit(id, partneredProviderRequestUpdate)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = partneredProviderService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = partneredProviderService.findById(id)
}