package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.dto.request.PartneredRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredRequestUpdate
import com.pucminas.authservice.web.service.PartneredService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(PartneredRestController.URL)
class PartneredRestController(private val partneredService: PartneredService) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(AssociateRestController::class.java)
        const val URL = "/partnered"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody partneredRequest: PartneredRequestCreate) =
        partneredService.save(partneredRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody partneredRequest: PartneredRequestUpdate) =
        partneredService.edit(id, partneredRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = partneredService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = partneredService.findById(id)

}