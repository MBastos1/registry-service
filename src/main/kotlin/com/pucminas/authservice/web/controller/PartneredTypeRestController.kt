package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.service.PartneredTypeService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(PartneredTypeRestController.URL)
class PartneredTypeRestController(private val partneredTypeService: PartneredTypeService) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(PartneredTypeRestController::class.java)
        const val URL = "/partnered-type"
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String) = partneredTypeService.findById(id)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = partneredTypeService.findAll()
}