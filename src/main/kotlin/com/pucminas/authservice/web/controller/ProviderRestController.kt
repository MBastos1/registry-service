package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.dto.request.AssociateRequest
import com.pucminas.authservice.web.dto.request.ProviderRequest
import com.pucminas.authservice.web.dto.request.ProviderRequestCreate
import com.pucminas.authservice.web.dto.request.ProviderRequestUpdate
import com.pucminas.authservice.web.service.ProviderService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(ProviderRestController.URL)
class ProviderRestController(private val providerService: ProviderService) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(ProviderRestController::class.java)
        const val URL = "/provider"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody providerRequest: ProviderRequestCreate) =
        providerService.save(providerRequest)

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable id: String, @RequestBody providerRequest: ProviderRequestUpdate) =
        providerService.edit(id, providerRequest)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getAll() = providerService.findAll()

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable id: String) = providerService.findById(id)

    @GetMapping("/person/{personId}")
    @ResponseStatus(HttpStatus.OK)
    fun findByPersonId(@PathVariable personId: String) = providerService.findByPersonId(personId)

    @GetMapping("/person/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun findByUserId(@PathVariable userId: String) = providerService.findByPersonUserId(userId)
}