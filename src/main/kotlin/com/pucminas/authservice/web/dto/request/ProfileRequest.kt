package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class ProfileRequest(
    val id: String
)