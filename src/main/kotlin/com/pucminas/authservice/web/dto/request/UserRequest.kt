package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class UserRequest(
    val email: String,
    val password: String,
    val manager: Boolean,
    val note: String?,
    val profileId: String
)