package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class PartneredTypeResponse (
    val id: String?,
    val description: String?,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime? = null
)
