package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
class PartneredProviderRequestUpdate (
    @JsonIgnore val id: String,
    override val partneredId: String,
    override val providerId: String,
    override val note: String?,
    override val status: Boolean = true,
    val createdAt: LocalDateTime
) : PartneredProviderRequest(partneredId, providerId, note, status)