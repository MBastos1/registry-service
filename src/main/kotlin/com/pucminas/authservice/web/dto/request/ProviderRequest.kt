package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
abstract class ProviderRequest(
    open val note: String?,
    open val status: Boolean? = true,
    open val councilNumber: String?,
    open val person: PersonRequest
)
