package com.pucminas.authservice.web.dto.response

import java.time.LocalDate
import java.time.LocalDateTime

class PersonResponse(
    val id: String,
    val user: UserResponse,
    val name: String,
    val mother: String,
    val birthDate: LocalDate,
    val cpf: String,
    val phone: String?,
    val cellPhone: String?,
    val phoneResponsible: String?,
    val email: String?,
    val gender: String,
    val maritalStatus: String?,
    val agency: String?,
    val account: String?,
    val bank: String?,
    val cep: String?,
    val city: String?,
    val district: String?,
    val street: String?,
    val number: Int?,
    val uf: String?,
    val complement: String?,
    val latitude: String?,
    val longitude: String?,
    val category: CategoryResponse,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime? = null
)