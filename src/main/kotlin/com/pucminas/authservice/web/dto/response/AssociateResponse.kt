package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class AssociateResponse(
    val id: String?,
    val cardNumber: String?,
    val holder: Boolean?,
    val dependent: Boolean?,
    val note: String?,
    val person: PersonResponse,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime? = null
)
