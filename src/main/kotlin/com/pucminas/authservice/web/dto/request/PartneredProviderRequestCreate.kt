package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class PartneredProviderRequestCreate (
    override val partneredId: String,
    override val providerId: String,
    override val note: String?,
    override val status: Boolean = true
) : PartneredProviderRequest(partneredId, providerId, note, status)