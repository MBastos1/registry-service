package com.pucminas.authservice.web.dto.response

class PartneredProviderResponse (
    val partnered: PartneredResponse,
    val provider: ProviderResponse,
    val note: String?,
    val status: Boolean? = true
)
