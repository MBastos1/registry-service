package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class CompanyResponse (
    val id: String?,
    val socialReason: String?,
    val fantasyName: String?,
    val cnpj: String?,
    val contactName: String?,
    val contactPhone: String?,
    val address: String?,
    val isActive: Boolean = true,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime?
)
