package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

abstract class BaseResponse (
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime? = null
)