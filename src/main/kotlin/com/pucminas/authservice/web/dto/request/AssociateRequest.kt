package com.pucminas.authservice.web.dto.request

abstract class AssociateRequest(
    open val cardNumber: String?,
    open val holder: Boolean?,
    open val dependent: Boolean?,
    open val note: String?,
    open val person: PersonRequest
)