package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
class AssociateRequestUpdate(
    @JsonIgnore val id: String? = null,
    override val cardNumber: String?,
    override val holder: Boolean?,
    override val dependent: Boolean?,
    override val note: String?,
    override val person: PersonRequestUpdate,
    val createdAt: LocalDateTime?,
) : AssociateRequest(cardNumber, holder, dependent, note, person)