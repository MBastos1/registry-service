package com.pucminas.authservice.web.dto.request

import java.time.LocalDateTime

class ProviderRequestUpdate(
    override val note: String?,
    override val status: Boolean? = true,
    override val councilNumber: String?,
    override val person: PersonRequestUpdate,
    val createdAt: LocalDateTime
) : ProviderRequest (note, status, councilNumber, person)