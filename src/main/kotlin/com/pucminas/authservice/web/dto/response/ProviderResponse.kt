package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class ProviderResponse(
    val id: String,
    val note: String?,
    val status: Boolean? = true,
    val councilNumber: String?,
    val person: PersonResponse,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime? = null
)
