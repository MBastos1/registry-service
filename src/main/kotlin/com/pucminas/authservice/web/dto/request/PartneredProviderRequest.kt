package com.pucminas.authservice.web.dto.request

abstract class PartneredProviderRequest (
    open val partneredId: String,
    open val providerId: String,
    open val note: String?,
    open val status: Boolean = true
)