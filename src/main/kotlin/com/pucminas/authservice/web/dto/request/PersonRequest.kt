package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDate

@JsonInclude(JsonInclude.Include.NON_NULL)
abstract class PersonRequest(
    open val name: String,
    open val mother: String,
    open val birthDate: LocalDate,
    open val cpf: String,
    open val phone: String?,
    open val cellPhone: String?,
    open val phoneResponsible: String?,
    open val email: String?,
    open val gender: String,
    open val maritalStatus: String?,
    open val agency: String?,
    open val account: String?,
    open val bank: String?,
    open val cep: String?,
    open val city: String?,
    open val district: String?,
    open val street: String?,
    open val number: Int?,
    open val uf: String?,
    open val complement: String?,
    open val latitude: String?,
    open val longitude: String?,
    open val categoryId: String,
)