package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class DiagramResponse (
    val id: String?,
    val processName: String?,
    val fileName: String?,
    val canvasHeight: String?,
    val xml: String?,
    val isActive: Boolean = false,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime?
)
