package com.pucminas.authservice.web.dto.request

class DiagramRequest (
    val processName: String,
    val fileName: String,
    val canvasHeight: String,
    val xml: String,
    val isActive: Boolean = false,
)
