package com.pucminas.authservice.web.dto.request

import java.time.LocalDate

class PersonRequestCreate(
    val user: UserRequest,
    override val name: String,
    override val mother: String,
    override val birthDate: LocalDate,
    override val cpf: String,
    override val phone: String?,
    override val cellPhone: String?,
    override val phoneResponsible: String?,
    override val email: String?,
    override val gender: String,
    override val maritalStatus: String?,
    override val agency: String?,
    override val account: String?,
    override val bank: String?,
    override val cep: String?,
    override val city: String?,
    override val district: String?,
    override val street: String?,
    override val number: Int?,
    override val uf: String?,
    override val complement: String?,
    override val latitude: String?,
    override val longitude: String?,
    override val categoryId: String,
) : PersonRequest(
    name,
    mother,
    birthDate,
    cpf,
    phone,
    cellPhone,
    phoneResponsible,
    email,
    gender,
    maritalStatus,
    agency,
    account,
    bank,
    cep,
    city,
    district,
    street,
    number,
    uf,
    complement,
    latitude,
    longitude,
    categoryId
)

