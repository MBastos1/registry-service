package com.pucminas.authservice.web.dto.request

import java.time.LocalDateTime

class ProviderRequestCreate(
    override val note: String?,
    override val status: Boolean? = true,
    override val councilNumber: String?,
    override val person: PersonRequestCreate,
    val createdAt: LocalDateTime?
) : ProviderRequest(note, status, councilNumber, person)