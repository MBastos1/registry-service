package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
class PartneredRequestUpdate (
    @JsonIgnore val id: String?,
    val partneredTypeId: String,
    val socialReason: String,
    val fantasyName: String,
    val cnpj: String,
    val email: String,
    val contactName: String,
    val phoneContactName: String,
    val agency: String,
    val account: String,
    val bank: String,
    val cep: String?,
    val city: String?,
    val district: String?,
    val street: String?,
    val number: Int?,
    val uf: String?,
    val complement: String?,
    val latitude: String?,
    val longitude: String?,
    val active: Boolean?,
    val createdAt: LocalDateTime
)