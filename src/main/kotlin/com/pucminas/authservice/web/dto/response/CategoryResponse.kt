package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class CategoryResponse(
    val id: String?,
    val description: String?,
    val detail: String?,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime? = null
)
