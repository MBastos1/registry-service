package com.pucminas.authservice.web.dto.response

import java.time.LocalDateTime

class PartneredResponse (
    val id: String?,
    val socialReason: String,
    val fantasyName: String,
    val cnpj: String,
    val email: String,
    val contactName: String,
    val phoneContactName: String,
    val agency: String,
    val account: String,
    val bank: String,
    val cep: String?,
    val city: String?,
    val district: String?,
    val street: String?,
    val number: Int?,
    val uf: String?,
    val complement: String?,
    val latitude: String?,
    val longitude: String?,
    val active: Boolean?,
    val partneredTypeResponse: PartneredTypeResponse,
    val createdAt: LocalDateTime?,
    val updatedAt: LocalDateTime? = null
)