package com.pucminas.authservice.web.dto.response

data class ProfileResponse(
    val description: String
)
