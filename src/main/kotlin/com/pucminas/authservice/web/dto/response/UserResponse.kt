package com.pucminas.authservice.web.dto.response

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.LocalDateTime

data class UserResponse(
    val id: String,
    @JsonIgnore
    val name: String? = null,
    val email: String,
    val manager: Boolean,
    val note: String? = null,
    val profile: ProfileResponse
)
