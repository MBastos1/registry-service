package com.pucminas.authservice.web.dto.request

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class AssociateRequestCreate(
    override val cardNumber: String?,
    override val holder: Boolean?,
    override val dependent: Boolean?,
    override val note: String?,
    override val person: PersonRequestCreate,
    val healthInsuranceRequest: HealthInsuranceRequest
) : AssociateRequest(cardNumber, holder, dependent, note, person)