package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Category
import com.pucminas.authservice.web.dto.response.CategoryResponse


fun Category.toResponse() = CategoryResponse(
    id = this.id,
    description = this.description,
    detail = this.detail,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)