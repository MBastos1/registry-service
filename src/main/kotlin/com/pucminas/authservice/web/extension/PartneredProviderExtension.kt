package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Partnered
import com.pucminas.authservice.domain.entity.PartneredProvider
import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredProviderResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import java.time.LocalDateTime

fun PartneredProviderRequestCreate.toEntity(partnered: Partnered, provider: Provider) = PartneredProvider(
    partnered = partnered,
    provider = provider,
    note = this.note,
    status = this.status,
    createdAt = LocalDateTime.now()
)

fun PartneredProviderRequestUpdate.toEntity(partnered: Partnered, provider: Provider) = PartneredProvider(
    id = this.id,
    partnered = partnered,
    provider = provider,
    note = this.note,
    status = this.status,
    createdAt = this.createdAt,
    updatedAt = LocalDateTime.now()
)

fun PartneredProvider.toResponse(userResponse: UserResponse) = PartneredProviderResponse(
    partnered = partnered.toResponse(),
    provider = provider.toResponse(userResponse),
    note = this.note,
    status = this.status
)