package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Partnered
import com.pucminas.authservice.domain.entity.PartneredType
import com.pucminas.authservice.web.dto.request.PartneredRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredResponse
import java.time.LocalDateTime

fun PartneredRequestCreate.toEntity() = Partnered(
    socialReason = socialReason,
    fantasyName = fantasyName,
    cnpj = cnpj,
    email = email,
    contactName = contactName,
    phoneContactName = phoneContactName,
    agency = agency,
    account = account,
    bank = bank,
    cep = cep,
    city = city,
    district = district,
    street = street,
    number = number,
    uf = uf,
    complement = complement,
    latitude = latitude,
    longitude = longitude,
    active = active,
    partneredType = PartneredType(
        id = this.partneredTypeId
    ),
    createdAt = LocalDateTime.now()
)

fun PartneredRequestUpdate.toEntity(id: String) = Partnered(
    id = id,
    socialReason = socialReason,
    fantasyName = fantasyName,
    cnpj = cnpj,
    email = email,
    contactName = contactName,
    phoneContactName = phoneContactName,
    agency = agency,
    account = account,
    bank = bank,
    cep = cep,
    city = city,
    district = district,
    street = street,
    number = number,
    uf = uf,
    complement = complement,
    latitude = latitude,
    longitude = longitude,
    active = active,
    partneredType = PartneredType(
        id = this.partneredTypeId
    ),
    createdAt = createdAt,
    updatedAt = LocalDateTime.now()
)

fun Partnered.toResponse() = PartneredResponse(
    id,
    socialReason,
    fantasyName,
    cnpj,
    email,
    contactName,
    phoneContactName,
    agency,
    account,
    bank,
    cep,
    city,
    district,
    street,
    number,
    uf,
    complement,
    latitude,
    longitude,
    active,
    partneredType.toResponse(),
    createdAt,
    updatedAt
)