package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Diagram
import com.pucminas.authservice.web.dto.request.DiagramRequest
import com.pucminas.authservice.web.dto.response.DiagramResponse
import java.util.*

fun DiagramRequest.toEntity(id: String?) = Diagram(
    id = id ?: UUID.randomUUID().toString(),
    processName = this.processName,
    fileName = this.fileName,
    canvasHeight = this.canvasHeight,
    xml = this.xml,
    isActive = this.isActive
)


fun Diagram.toResponse() = DiagramResponse(
    id = this.id,
    processName = this.processName,
    fileName = this.fileName,
    canvasHeight = this.canvasHeight,
    xml = this.xml,
    isActive = this.isActive,
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)