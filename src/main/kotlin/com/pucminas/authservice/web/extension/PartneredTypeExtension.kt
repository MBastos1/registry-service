package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.PartneredType
import com.pucminas.authservice.web.dto.response.PartneredTypeResponse

fun PartneredType.toResponse() = PartneredTypeResponse(
    id, description, createdAt, updatedAt
)