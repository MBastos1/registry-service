package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Category
import com.pucminas.authservice.domain.entity.Person
import com.pucminas.authservice.web.dto.request.PersonRequest
import com.pucminas.authservice.web.dto.request.PersonRequestCreate
import com.pucminas.authservice.web.dto.request.PersonRequestUpdate
import com.pucminas.authservice.web.dto.response.CategoryResponse
import com.pucminas.authservice.web.dto.response.PersonResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import java.time.LocalDateTime


fun PersonRequestCreate.toEntity(userId: String) = Person(
    userId = userId,
    name = this.name,
    mother = this.mother,
    birthDate = this.birthDate,
    cpf = this.cpf,
    phone = this.phone,
    cellPhone = this.cellPhone,
    phoneResponsible = this.phoneResponsible,
    email = this.email,
    gender = this.gender,
    maritalStatus = this.maritalStatus,
    agency = this.agency,
    account = this.account,
    bank = this.bank,
    cep = this.cep,
    city = this.city,
    district = this.district,
    street = this.street,
    number = this.number,
    uf = this.uf,
    complement = this.complement,
    latitude = this.latitude,
    longitude = this.longitude,
    category = Category(
        id = this.categoryId
    ),
    personSchooling = emptyList(),
    createdAt = LocalDateTime.now()
)

fun PersonRequestUpdate.toEntity(userId: String) = Person(
    id = this.id,
    userId = userId,
    name = this.name,
    mother = this.mother,
    birthDate = this.birthDate,
    cpf = this.cpf,
    phone = this.phone,
    cellPhone = this.cellPhone,
    phoneResponsible = this.phoneResponsible,
    email = this.email,
    gender = this.gender,
    maritalStatus = this.maritalStatus,
    agency = this.agency,
    account = this.account,
    bank = this.bank,
    cep = this.cep,
    city = this.city,
    district = this.district,
    street = this.street,
    number = this.number,
    uf = this.uf,
    complement = this.complement,
    latitude = this.latitude,
    longitude = this.longitude,
    category = Category(
        id = this.categoryId
    ),
    personSchooling = emptyList(),
    createdAt = this.createdAt,
    updatedAt = LocalDateTime.now()
)

fun Person.toResponse(userResponse: UserResponse) = PersonResponse(
    id = this.id,
    user = userResponse,
    name = this.name,
    mother = this.mother,
    birthDate = this.birthDate,
    cpf = this.cpf,
    phone = this.phone,
    cellPhone = this.cellPhone,
    phoneResponsible = this.phoneResponsible,
    email = this.email,
    gender = this.gender,
    maritalStatus = this.maritalStatus,
    agency = this.agency,
    account = this.account,
    bank = this.bank,
    cep = this.cep,
    city = this.city,
    district = this.district,
    street = this.street,
    number = this.number,
    uf = this.uf,
    complement = this.complement,
    latitude = this.latitude,
    longitude = this.longitude,
    category = this.category.toResponse(),
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)