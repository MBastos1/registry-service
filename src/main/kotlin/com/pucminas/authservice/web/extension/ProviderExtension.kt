package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.web.dto.request.ProviderRequest
import com.pucminas.authservice.web.dto.request.ProviderRequestCreate
import com.pucminas.authservice.web.dto.request.ProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.ProviderResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import java.time.LocalDateTime
import java.util.*

fun ProviderRequestCreate.toEntity(userId: String) = Provider(
    id = UUID.randomUUID().toString(),
    note = this.note,
    status = this.status,
    councilNumber = this.councilNumber,
    person = this.person.toEntity(userId),
    createdAt = LocalDateTime.now()
)

fun ProviderRequestUpdate.toEntity(id: String, userId: String) = Provider(
    id = id,
    note = this.note,
    status = this.status,
    councilNumber = this.councilNumber,
    person = this.person.toEntity(userId),
    createdAt = this.createdAt,
    updatedAt = LocalDateTime.now()
)

fun Provider.toResponse(userResponse: UserResponse) = ProviderResponse(
    id = this.id,
    note = this.note,
    status = this.status,
    councilNumber = this.councilNumber,
    person = this.person.toResponse(userResponse),
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)