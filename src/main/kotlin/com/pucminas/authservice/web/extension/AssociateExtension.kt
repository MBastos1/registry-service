package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.Associate
import com.pucminas.authservice.web.dto.request.AssociateRequest
import com.pucminas.authservice.web.dto.request.AssociateRequestCreate
import com.pucminas.authservice.web.dto.request.AssociateRequestUpdate
import com.pucminas.authservice.web.dto.response.AssociateResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import java.time.LocalDateTime
import java.util.*

fun AssociateRequestCreate.toEntity(userId: String) = Associate(
    id = UUID.randomUUID().toString(),
    cardNumber = this.cardNumber,
    holder = this.holder,
    dependent = this.dependent,
    note = this.note,
    person = this.person.toEntity(userId = userId),
    createdAt = LocalDateTime.now()
)

fun AssociateRequestUpdate.toEntity(id: String, userId: String) = Associate(
    id = id,
    cardNumber = this.cardNumber,
    holder = this.holder,
    dependent = this.dependent,
    note = this.note,
    person = this.person.toEntity(userId = userId),
    createdAt = this.createdAt,
    updatedAt = LocalDateTime.now()
)

fun Associate.toResponse(userResponse: UserResponse) = AssociateResponse(
    id = this.id,
    cardNumber = this.cardNumber,
    holder = this.holder,
    dependent = this.dependent,
    note = this.note,
    person = person.toResponse(userResponse),
    createdAt = this.createdAt,
    updatedAt = this.updatedAt
)