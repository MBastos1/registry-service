package com.pucminas.authservice.web.extension

import com.pucminas.authservice.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.authservice.web.dto.request.HealthInsuranceRequest


fun HealthInsuranceRequest.toRequest(associateId: String) = HealthInsuranceCreateRequest(
    planId = this.planId,
    associateId = associateId,
    note = this.note,
    companyId = null,
    isActive = this.isActive,
    appointmentsServiceLimit = this.appointmentsServiceLimit,
    serviceLimitExams = this.serviceLimitExams,
    contractStartDate = this.contractStartDate,
    contractEndDate = this.contractEndDate,
    isRenewedContract = this.isRenewedContract
)