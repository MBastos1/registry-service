package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.apache.logging.log4j.util.Strings
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Category(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    override val id: String = UUID.randomUUID().toString(),

    @Column(length = 50, nullable = true)
    val description: String? = Strings.EMPTY,

    @Column(length = 50, nullable = true)
    val detail: String? = Strings.EMPTY,
) : BaseEntity()