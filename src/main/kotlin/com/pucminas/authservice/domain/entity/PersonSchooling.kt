package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "person_schooling")
data class PersonSchooling(

    @Column(length = 50, nullable = true)
    val description: String?,

    @ManyToOne
    val person: Person
) : BaseEntity()