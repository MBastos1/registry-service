package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Partnered(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 100, nullable = false)
    val socialReason: String,

    @Column(length = 100, nullable = false)
    val fantasyName: String,

    @Column(length = 20, nullable = false)
    val cnpj: String,

    @Column(length = 100, nullable = false)
    val email: String,

    @Column(length = 100, nullable = false)
    val contactName: String,

    @Column(length = 100, nullable = false)
    val phoneContactName: String,

    @Column(length = 10, nullable = false)
    val agency: String,

    @Column(length = 10, nullable = false)
    val account: String,

    @Column(length = 10, nullable = false)
    val bank: String,

    @Column(length = 10, nullable = true)
    val cep: String?,

    @Column(length = 100, nullable = true)
    val city: String?,

    @Column(length = 100, nullable = true)
    val district: String?,

    @Column(length = 100, nullable = true)
    val street: String?,

    @Column(nullable = true)
    val number: Int?,

    @Column(length = 2, nullable = true)
    val uf: String?,

    @Column(length = 100, nullable = true)
    val complement: String?,

    @Column(length = 20, nullable = true)
    val latitude: String?,

    @Column(length = 20, nullable = true)
    val longitude: String?,

    @Column(nullable = false)
    val active: Boolean?,

    @ManyToOne
    val partneredType: PartneredType,

    @CreationTimestamp
    @Column(updatable = false, insertable = true)
    val createdAt: LocalDateTime,

    @Column(updatable = true, insertable = false, nullable = true)
    val updatedAt: LocalDateTime? = null
)