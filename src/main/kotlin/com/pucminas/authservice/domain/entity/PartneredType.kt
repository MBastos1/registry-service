package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class PartneredType(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    override val id: String = UUID.randomUUID().toString(),

    @Column(length = 50, nullable = true)
    val description: String? = null
) : BaseEntity()