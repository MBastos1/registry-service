package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Person(

    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 36, nullable = false)
    val userId: String,

    @Column(length = 150, nullable = false)
    val name: String,

    @Column(length = 150, nullable = false)
    val mother: String,

    @Column(nullable = false)
    val birthDate: LocalDate,

    @Column(length = 11, nullable = false)
    val cpf: String,

    @Column(length = 11, nullable = true)
    val phone: String?,

    @Column(length = 11, nullable = true)
    val cellPhone: String?,

    @Column(length = 11, nullable = true)
    val phoneResponsible: String?,

    @Column(length = 150, nullable = true)
    val email: String?,

    @Column(length = 1, nullable = false)
    val gender: String,

    @Column(length = 20, nullable = true)
    val maritalStatus: String?,

    @Column(length = 10, nullable = true)
    val agency: String?,

    @Column(length = 10, nullable = true)
    val account: String?,

    @Column(length = 10, nullable = true)
    val bank: String?,

    @Column(length = 10, nullable = true)
    val cep: String?,

    @Column(length = 100, nullable = true)
    val city: String?,

    @Column(length = 100, nullable = true)
    val district: String?,

    @Column(length = 100, nullable = true)
    val street: String?,

    @Column(nullable = true)
    val number: Int?,

    @Column(length = 2, nullable = true)
    val uf: String?,

    @Column(length = 100, nullable = true)
    val complement: String?,

    @Column(length = 20, nullable = true)
    val latitude: String?,

    @Column(length = 20, nullable = true)
    val longitude: String?,

    @OneToOne
    val category: Category,

    @OneToMany(mappedBy = "person")
    val personSchooling: List<PersonSchooling>? = null,

    @CreationTimestamp
    @Column(updatable = false, insertable = true)
    val createdAt: LocalDateTime?,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    val updatedAt: LocalDateTime? = null
)