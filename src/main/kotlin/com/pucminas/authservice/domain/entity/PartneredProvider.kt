package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class PartneredProvider(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @OneToOne
    val partnered: Partnered,

    @OneToOne
    val provider: Provider,

    @Column(length = 200, nullable = true)
    val note: String?,

    @Column(nullable = false)
    val status: Boolean?,

    @CreationTimestamp
    @Column(updatable = false, insertable = true)
    val createdAt: LocalDateTime,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
)