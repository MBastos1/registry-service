package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Associate(

    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 20, nullable = true)
    var cardNumber: String?,

    @Column(nullable = true)
    var holder: Boolean? = false,

    @Column(length = 20, nullable = true)
    var dependent: Boolean? = false,

    @Column(length = 200, nullable = true)
    var note: String? = null,

    @OneToOne(optional = false, fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val person: Person,

    @CreationTimestamp
    @Column(updatable = false, insertable = true)
    val createdAt: LocalDateTime?,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    val updatedAt: LocalDateTime? = null
)