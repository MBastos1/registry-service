package com.pucminas.authservice.domain.entity

import org.apache.logging.log4j.util.Strings
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.Nationalized
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Lob

@Entity
class Diagram (
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 100, nullable = false)
    val processName: String? = Strings.EMPTY,

    @Column(length = 50, nullable = true)
    val fileName: String? = Strings.EMPTY,

    @Column(length = 50, nullable = true)
    val canvasHeight: String? = Strings.EMPTY,

    @Column(columnDefinition="text", nullable = false)
    val xml: String? = Strings.EMPTY,

    val isActive: Boolean = false
){
    @CreationTimestamp
    @Column(updatable = false)
    lateinit var createdAt: LocalDateTime

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    var updatedAt: LocalDateTime? = null
}
