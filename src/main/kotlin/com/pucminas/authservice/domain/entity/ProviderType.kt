package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity

@Entity
data class ProviderType(
    @Column(length = 50, nullable = true)
    val description: String?
) : BaseEntity()