package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Provider(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String = UUID.randomUUID().toString(),

    @Column(length = 200, nullable = true)
    val note: String?,

    @Column(nullable = false)
    val status: Boolean?,

    @Column(length = 50, nullable = true)
    val councilNumber: String?,

    @OneToOne(optional = false, fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val person: Person,

    @CreationTimestamp
    @Column(updatable = false)
    val createdAt: LocalDateTime,

    @UpdateTimestamp
    @Column(updatable = true, insertable = false, nullable = true)
    val updatedAt: LocalDateTime? = null
)
