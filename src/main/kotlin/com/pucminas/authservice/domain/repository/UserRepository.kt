package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.Person
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : BaseRepository<Person, String> {
    fun findByEmail(email: String): Person?
}