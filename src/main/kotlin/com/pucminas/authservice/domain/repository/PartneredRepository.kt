package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.Partnered
import org.springframework.stereotype.Repository

@Repository
interface PartneredRepository : BaseRepository<Partnered, String>