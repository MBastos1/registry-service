package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.PartneredProvider
import org.springframework.stereotype.Repository

@Repository
interface PartneredProviderRepository : BaseRepository<PartneredProvider, String>