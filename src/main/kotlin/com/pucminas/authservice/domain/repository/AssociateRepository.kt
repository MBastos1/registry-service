package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.entity.Associate
import com.pucminas.authservice.domain.base.BaseRepository
import java.util.*
import org.springframework.stereotype.Repository

@Repository
interface AssociateRepository : BaseRepository<Associate, String> {
    fun save(associate: Associate): Associate
    fun findByPersonId(personId: String): Optional<Associate>
    fun findByPersonUserId(userId: String): Optional<Associate>
}