package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.Diagram
import org.springframework.stereotype.Repository

@Repository
interface DiagramRepository : BaseRepository<Diagram, String>