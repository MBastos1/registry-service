package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.domain.base.BaseRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ProviderRepository : BaseRepository<Provider, String> {
    fun save(provider: Provider): Provider
    fun findByPersonId(personId: String): Optional<Provider>
    fun findByPersonUserId(userId: String): Optional<Provider>
}