package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.entity.Category
import com.pucminas.authservice.domain.base.BaseRepository
import org.springframework.stereotype.Repository

@Repository
interface CatergoryRepository : BaseRepository<Category, String>