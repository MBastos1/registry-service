package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.PartneredType
import org.springframework.stereotype.Repository

@Repository
interface PartneredTypeRepository : BaseRepository<PartneredType, String>