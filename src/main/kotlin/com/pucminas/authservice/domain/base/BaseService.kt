package com.pucminas.authservice.domain.base

import com.pucminas.authservice.web.dto.response.AssociateResponse
import java.util.*

interface BaseService<T, ID> {
    fun findById(id: ID): T?
    fun findAll(): List<T>
}