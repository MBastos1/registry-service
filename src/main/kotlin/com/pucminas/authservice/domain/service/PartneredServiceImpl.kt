package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.repository.PartneredRepository
import com.pucminas.authservice.web.dto.request.PartneredRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredResponse
import com.pucminas.authservice.web.extension.toEntity
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.PartneredService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class PartneredServiceImpl(
    private val partneredRepository: PartneredRepository
) : PartneredService {

    override fun save(partneredRequest: PartneredRequestCreate): PartneredResponse =
        runCatching {
            partneredRepository
                .save(partneredRequest.toEntity())
                .toResponse()
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, partneredRequest: PartneredRequestUpdate): PartneredResponse =
        partneredRepository.findById(id).map {
            partneredRepository
                .save(partneredRequest.toEntity(id))
                .toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Conveniado $id não encontrada")
        }

    override fun findById(id: String): PartneredResponse? =
        partneredRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Conveniado $id não encontrada")
        }

    override fun findAll(): List<PartneredResponse> =
        partneredRepository.findAll().map {
            it.toResponse()
        }
}