package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.repository.AssociateRepository
import com.pucminas.authservice.provider.AuthServiceProvider
import com.pucminas.authservice.provider.ScheduleServiceProvider
import com.pucminas.authservice.web.dto.request.AssociateRequest
import com.pucminas.authservice.web.dto.request.AssociateRequestCreate
import com.pucminas.authservice.web.dto.request.AssociateRequestUpdate
import com.pucminas.authservice.web.dto.response.AssociateResponse
import com.pucminas.authservice.web.dto.response.PersonResponse
import com.pucminas.authservice.web.extension.toEntity
import com.pucminas.authservice.web.extension.toRequest
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.AssociateService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AssociateServiceImpl(
    private val associateRepository: AssociateRepository,
    private val authServiceProvider: AuthServiceProvider,
    private val scheduleServiceProvider: ScheduleServiceProvider
) : AssociateService {

    override fun save(associateRequest: AssociateRequestCreate): AssociateResponse =
        runCatching {
            val userResponse = authServiceProvider.saveUser(associateRequest.person.user)
            with(associateRepository.save(associateRequest.toEntity(userResponse.id))) {
                scheduleServiceProvider.save(
                    associateRequest.healthInsuranceRequest.toRequest(this.id)
                )
                this.toResponse(userResponse)
            }
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, associateRequest: AssociateRequestUpdate): AssociateResponse =
        associateRepository.findById(id).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            associateRepository
                .save(associateRequest.toEntity(id, userResponse.id))
                .toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Associado $id não encontrada")
        }

    override fun findByPersonUserId(userId: String): PersonResponse =
        associateRepository.findByPersonUserId(userId).map {
            val userResponse = authServiceProvider.findById(userId)
            it.person.toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Associado $userId não encontrada")
        }

    override fun findByPersonId(personId: String): PersonResponse =
        associateRepository.findByPersonId(personId).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.person.toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Associado $personId não encontrada")
        }

    override fun findById(id: String): AssociateResponse? =
        associateRepository.findById(id).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Associado $id não encontrada")
        }

    override fun findAll(): List<AssociateResponse> =
        associateRepository.findAll().map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.toResponse(userResponse)
        }
}

