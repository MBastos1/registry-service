package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.repository.PartneredRepository
import com.pucminas.authservice.domain.repository.PartneredTypeRepository
import com.pucminas.authservice.web.dto.response.PartneredTypeResponse
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.PartneredTypeService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class PartneredTypeServiceImpl(private val partneredTypeRepository: PartneredTypeRepository) : PartneredTypeService {

    override fun findById(id: String): PartneredTypeResponse? =
        partneredTypeRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Tipo conveniado $id não encontrada")
        }

    override fun findAll(): List<PartneredTypeResponse> =
        partneredTypeRepository.findAll().map {
            it.toResponse()
        }
}