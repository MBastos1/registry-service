package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.entity.Category
import com.pucminas.authservice.domain.repository.CatergoryRepository
import com.pucminas.authservice.web.dto.response.CategoryResponse
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.CategoryService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class CategoryServiceImpl(private val categoryRepository: CatergoryRepository) : CategoryService {

    override fun findById(id: String): CategoryResponse? =
        categoryRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Categoria $id não encontrada")
        }

    override fun findAll(): List<CategoryResponse> =
        categoryRepository.findAll().map {
            it.toResponse()
        }
}