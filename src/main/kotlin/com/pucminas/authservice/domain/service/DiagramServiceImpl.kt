package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.repository.DiagramRepository
import com.pucminas.authservice.web.dto.request.DiagramRequest
import com.pucminas.authservice.web.dto.response.DiagramResponse
import com.pucminas.authservice.web.extension.toEntity
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.DiagramService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class DiagramServiceImpl(private val diagramRepository: DiagramRepository) : DiagramService {

    override fun save(diagramRequest: DiagramRequest): DiagramResponse =
        runCatching {
            diagramRepository.save(diagramRequest.toEntity(null)).toResponse()
        }.getOrElse {
            throw it
        }

    override fun edit(id: String, diagramRequest: DiagramRequest): DiagramResponse =
        diagramRepository.findById(id).map {
            diagramRepository
                .save(diagramRequest.toEntity(id))
                .toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Diagrama $id não encontrada")
        }

    override fun findById(id: String): DiagramResponse? =
        diagramRepository.findById(id).map {
            it.toResponse()
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Diagram $id não encontrada")
        }

    override fun findAll(): List<DiagramResponse> =
        diagramRepository.findAll().map {
            it.toResponse()
        }

    override fun deleteById(id: String) {
        diagramRepository.findById(id).map {
            diagramRepository.deleteById(id)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Diagram $id não encontrada")
        }
    }
}