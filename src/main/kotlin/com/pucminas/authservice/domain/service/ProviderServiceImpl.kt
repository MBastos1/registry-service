package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.entity.Person
import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.domain.repository.ProviderRepository
import com.pucminas.authservice.provider.AuthServiceProvider
import com.pucminas.authservice.web.dto.request.ProviderRequest
import com.pucminas.authservice.web.dto.request.ProviderRequestCreate
import com.pucminas.authservice.web.dto.request.ProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.PersonResponse
import com.pucminas.authservice.web.dto.response.ProviderResponse
import com.pucminas.authservice.web.extension.toEntity
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.ProviderService
import javassist.NotFoundException
import org.springframework.http.HttpStatus
import java.util.*
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ProviderServiceImpl(
    private val providerRepository: ProviderRepository,
    private val authServiceProvider: AuthServiceProvider
) : ProviderService {

    override fun save(providerRequest: ProviderRequestCreate): ProviderResponse = runCatching {
        val userResponse = authServiceProvider.saveUser(providerRequest.person.user)
        providerRepository
            .save(providerRequest.toEntity(userResponse.id))
            .toResponse(userResponse)
    }.getOrElse {
        throw it
    }

    override fun edit(id: String, providerRequest: ProviderRequestUpdate): ProviderResponse =
        providerRepository.findById(id).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            providerRepository
                .save(providerRequest.toEntity(id, userResponse.id))
                .toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Provedor $id não encontrada")
        }

    override fun findByPersonUserId(userId: String): PersonResponse =
        providerRepository.findByPersonUserId(userId).map {
            val userResponse = authServiceProvider.findById(userId)
            it.person.toResponse(userResponse)
        }.orElseThrow {
            NotFoundException("Pessoa $userId não encontrada")
        }

    override fun findByPersonId(personId: String): PersonResponse =
        providerRepository.findByPersonId(personId).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.person.toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Provedor $personId não encontrado")
        }

    override fun findById(id: String): ProviderResponse =
        providerRepository.findById(id).map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.toResponse(userResponse)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Provedor $id não encontrada")
        }

    override fun findAll(): List<ProviderResponse> =
        providerRepository.findAll().map {
            val userResponse = authServiceProvider.findById(it.person.userId)
            it.toResponse(userResponse)
        }
}