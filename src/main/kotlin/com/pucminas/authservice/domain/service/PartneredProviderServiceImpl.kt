package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.entity.Partnered
import com.pucminas.authservice.domain.entity.PartneredProvider
import com.pucminas.authservice.domain.entity.Provider
import com.pucminas.authservice.domain.repository.PartneredProviderRepository
import com.pucminas.authservice.domain.repository.PartneredRepository
import com.pucminas.authservice.domain.repository.ProviderRepository
import com.pucminas.authservice.provider.AuthServiceProvider
import com.pucminas.authservice.web.dto.request.PartneredProviderRequest
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestCreate
import com.pucminas.authservice.web.dto.request.PartneredProviderRequestUpdate
import com.pucminas.authservice.web.dto.response.PartneredProviderResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import com.pucminas.authservice.web.extension.toEntity
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.PartneredProviderService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class PartneredProviderServiceImpl(
    private val partneredRepository: PartneredRepository,
    private val providerRepository: ProviderRepository,
    private val authServiceProvider: AuthServiceProvider,
    private val partneredProviderRepository: PartneredProviderRepository
) : PartneredProviderService {

    override fun save(partneredProviderRequest: PartneredProviderRequestCreate): PartneredProviderResponse =
        kotlin.runCatching {
            with(partneredProviderRequest) {
                val (partnered, provider, userResponse) = findObjects()
                partneredProviderRepository.save(
                    this.toEntity(
                        partnered = partnered,
                        provider = provider
                    )
                ).toResponse(userResponse)
            }
        }.getOrElse {
            throw it
        }

    private fun PartneredProviderRequest.findObjects(): Triple<Partnered, Provider, UserResponse> {
        val partnered = partneredRepository.findById(this.partneredId).get()
        val provider = providerRepository.findById(this.providerId).get()
        val userResponse = authServiceProvider.findById(provider.person.userId)
        return Triple(partnered, provider, userResponse)
    }

    override fun edit(id: String, partneredProviderRequest: PartneredProviderRequestUpdate): PartneredProviderResponse =
        partneredProviderRepository.findById(id).map {
            with(partneredProviderRequest) {
                val (partnered, provider, userResponse) = findObjects()
                partneredProviderRepository.save(
                    this.toEntity(
                        partnered = partnered,
                        provider = provider
                    )
                ).toResponse(userResponse)
            }
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Parceiro conveniado $id não encontrada")
        }

    override fun deleteById(id: String) {
        partneredProviderRepository.findById(id).map {
            partneredProviderRepository.deleteById(id)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Parceiro conveniado $id não encontrada")
        }
    }

    override fun findById(id: String): PartneredProviderResponse? =
        partneredProviderRepository.findById(id).map {
            toResponse(it)
        }.orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Parceiro conveniado $id não encontrada")
        }

    override fun findAll(): List<PartneredProviderResponse> =
        partneredProviderRepository.findAll().map {
            toResponse(it)
        }

    private fun toResponse(it: PartneredProvider): PartneredProviderResponse =
        it.toResponse(authServiceProvider.findById(it.provider.person.userId))
}