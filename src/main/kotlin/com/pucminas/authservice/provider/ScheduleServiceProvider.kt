package com.pucminas.authservice.provider

import com.pucminas.authservice.web.dto.request.HealthInsuranceCreateRequest
import com.pucminas.authservice.web.dto.request.HealthInsuranceUpdateRequest
import com.pucminas.authservice.web.dto.request.UserRequest
import com.pucminas.authservice.web.dto.response.HealthInsuranceResponse
import com.pucminas.authservice.web.dto.response.UserResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(
    value = "schedule",
    url = "http://\${SCHEDULE_SERVICE_URL:localhost}:8083"
)
interface ScheduleServiceProvider {

    @PostMapping("/api/v1/health-insurance")
    fun save(@RequestBody healthInsuranceCreateRequest: HealthInsuranceCreateRequest): HealthInsuranceResponse

    @PutMapping("/api/v1/health-insurance/{id}")
    fun edit(@PathVariable id: String, @RequestBody healthInsuranceUpdateRequest: HealthInsuranceUpdateRequest): HealthInsuranceResponse
}
