package com.pucminas.authservice.provider

import com.pucminas.authservice.web.dto.request.UserRequest
import com.pucminas.authservice.web.dto.response.UserResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.*

@FeignClient(
    value = "auth",
    url = "http://\${AUTH_SERVICE_URL:localhost}:8081"
)
interface AuthServiceProvider {

    @PostMapping("/api/v1/user")
    fun saveUser(@RequestBody userRequest: UserRequest): UserResponse

    @GetMapping("/api/v1/user/{userId}")
    fun findById(@PathVariable userId: String): UserResponse

    @PutMapping("/api/v1/user/{userId}")
    fun edit(@PathVariable userId: String, @RequestBody userRequest: UserRequest): UserResponse
}