-- Categories
INSERT INTO category (id, description, detail, created_at)
VALUES ('141ecbc1-7006-4ae3-831e-30fdbed02543','Ativo','Com plano em dia', getdate()) ;

INSERT INTO category (id, description, detail, created_at)
VALUES ('7bbaa990-9583-4bf5-9a48-216273d81729','Suspensos','Com plano em análise', getdate()) ;

INSERT INTO category (id, description, detail, created_at)
VALUES ('71c5266b-10b3-41af-a4a9-303bb71aa98e','Inativos','Ex-Clientes', getdate()) ;

-- Profiles
INSERT INTO provider_type (id, description, created_at)
VALUES ('e4c7e076-f593-4101-8296-527e66d0c11f','Médico', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('ab8b3964-8171-4328-8253-05a6be00db96','Enfermeiro', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('524473e5-2040-47de-bb98-038336e605a9','Dentista', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('0e7491ab-11d1-4de5-b1ad-1d86c59ef074','Fisioterapeuta', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('0b51da61-100d-4d1d-abb8-e160d5e788fe','Recepcionista', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('321e5703-b490-4963-b273-f221d4848079   ','Call center', getdate()) ;

INSERT INTO provider_type (id, description, created_at)
VALUES ('403c7857-0b3b-4b7b-8328-fadb77ebad35','Operação', getdate()) ;

-- Partnered Type
INSERT INTO partnered_type (id, description, created_at)
VALUES ('63add6eb-95fb-450b-998a-80d54b35ec1e','Hospital', getdate()) ;

INSERT INTO partnered_type (id, description, created_at)
VALUES ('ed2761c3-8830-49ea-bfb5-807a41a9a64a','Laboratório', getdate()) ;

INSERT INTO partnered_type (id, description, created_at)
VALUES ('2c786fe8-5174-48e7-8776-3a9752861e48','Clínica', getdate()) ;

-- Person
INSERT INTO person(
id, account, agency, bank, birth_date, cell_phone, cep, city, complement, cpf, district, email, gender, latitude,
longitude, marital_status, mother, name, [number], phone, phone_responsible, street, uf, user_id, category_id,
created_at, updated_at)
VALUES(
'0bc6f7f0-59bb-43cf-9f31-17ae685980ac',
'123456',
'001',
'001',
'1969-03-07',
'4064238168',
'59452',
'Hobson',
NULL,
'82526345243',
NULL,
NULL,
'M',
'19.9229',
'43.9926',
'Casado',
'Angelita Matteus Alfredsson',
'Gwyn Matteus Alfredsson',
4109,
'4069282776',
NULL,
'West Fork Street',
'MT',
'bd2a9cbf-f8c0-4ae2-9b18-caa76ae5a0a2',
'141ecbc1-7006-4ae3-831e-30fdbed02543',
getdate(),
NULL);

-- Associate
INSERT INTO associate(id, created_at, updated_at, card_number, dependent, holder, note, person_id)
VALUES('9d7283e4-8c01-43ef-8288-ae5f488d55df', getdate(), NULL, '5432123457896654', 0, 1, NULL, '0bc6f7f0-59bb-43cf-9f31-17ae685980ac');
