FROM openjdk:11-jdk-oraclelinux8
LABEL maintainer="pucminas.com"

VOLUME /tmp
EXPOSE 8082

ARG JAR_FILE=target/registry-service*.jar
ADD ${JAR_FILE} registry-service.jar

ENTRYPOINT ["sh", "-c", "java -jar /registry-service.jar"]

